set :application, 'RhubarbConfig'
set :repo_url, 'https://bitbucket.org/uits-ksd/katt-aws-rhubarb-config.git'

# Adding per https://github.com/rvm/rvm-capistrano#readme to use local Ruby version
set :rvm_ruby_string, :local

set :deploy_to, '/etc/opt/kuali/rhubarb/rhubarb-1.0'
set :scm, :git

set :format, :pretty
set :log_level, :debug
set :pty, true
