katt-kfs-rhubarb-config
=======================

### Description
This repository will hold all of the environment-specific configuration for emails that are sent from rhubarb. 

The email configuration .yaml files correspond to a folder or a job name in Control-M. Within these files, there are variables for email addresses; the email address values are defined on a per environment basis in the email/addresses folder.

### Building

Since we are wrapping our Rhubarb deployment within a Dockerfile, there are two considerations when defining the version of this code repository during the Docker image build.

1. The git branch that is being used when this project is cloned.
2. The "target environment". This is used to populate the RHUBARB_ENV for related rhubarb configuration. This used to correspond to a deployment stage when we were also using Capistrano (originating from on-premise deployments). For example, in the stg7.rb file, the master branch of the code is hard-coded. If you were doing any sort of "local" testing, the local7.rb file needed to be changed to match the feature branch of this code.

This is further explained in https://bitbucket.org/uits-ksd/docker-rhubarb/src/master/README.md.